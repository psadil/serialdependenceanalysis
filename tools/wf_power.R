
# run power analysis
devtools::load_all()
library(magrittr)

maked <- function(n, n_amplitude = 2){
  stopifnot(exprs = {
    (n%%2) == 0
    n_amplitude %in% c(1,2)
  })
  amplitudes <- rnorm(n_amplitude, 0, 0.5)
  w <- rgamma(n_amplitude, 2, 0.5)
  sigma <- rgamma(1, 2, 5)

  d <- tibble::tibble(trial = 1:n) %>%
    dplyr::mutate(
      amplitudes = purrr::map(trial, ~amplitudes),
      w = purrr::map(trial, ~w),
      sigma = sigma,
      orientation_diff = runif(dplyr::n(), -pi, pi),
      response_diff = runif(dplyr::n(), -pi, pi))

  mu <- dvm3(d$orientation_diff, a = amplitudes[1], w = w[1])
  if(n_amplitude == 2) mu <- mu + dvm3(d$response_diff, a = amplitudes[2], w = w[2])
  d$error <- mu + rnorm(n, 0, sd = sigma)

  return(d)
}

loo_power <- function(post,
                      data,
                      cores = 1) {

  draws <- post$draws()
  amplitude <- draws[,,stringr::str_detect(dimnames(draws)[[3]], "amplitude"), drop=FALSE]
  width <- draws[,,stringr::str_detect(dimnames(draws)[[3]], "width"), drop=FALSE]
  sigma <- draws[,,"sigma"]
  n_condition <- dim(amplitude)[3]

  LLarray <- array(dim = c(dim(draws)[1], dim(draws)[2], nrow(data)))
  for(chain in 1:dim(LLarray)[2]){
    for(draw in 1:dim(LLarray)[1]){
      w <- width[draw,chain,, drop=FALSE]
      a <- amplitude[draw,chain,, drop=FALSE]
      mu <- dvm3(data$orientation_diff, a = a[1], w = w[1])
      if(n_condition == 2) mu <- mu + dvm3(data$response_diff, a = a[2], w = w[2])
      LLarray[draw,chain,] <- dnorm(data$error, mean=mu, sd = sigma[draw,chain, 1], log=TRUE)
    }
  }

  r_eff <- loo::relative_eff(
    x = exp(LLarray),
    cores = cores)
  out <- loo::loo.array(
    LLarray,
    r_eff = r_eff,
    cores = cores)
  return(out)
}

do_stan <- function(d, full, stanfit0){
  stand <- d %>%
    dplyr::select(-amplitudes, -w) %>%
    tidybayes::compose_data()
  if(full) stand$n_condition <- 2
  else stand$n_condition <- 1

  post <- stanfit0$sample(
    data = stand,
    num_cores = 4,
    num_chains = 4,
    num_samples = 1000,
    num_warmup = 1000,
    output_dir = here::here("data-raw","samples"))

  return(post)
}

plan <- drake::drake_plan(
  j = drake::target(seq_len(100)),
  n = drake::target(c(5000, 10000, 15000)),
  n_amplitude = drake::target(c(1,2)),
  stanmodel = drake::target(
    cmdstanr::cmdstan_model(
      drake::ignore(here::here("src", "stan_files", "dvm_power.stan")),
      compiler_flags = drake::ignore(c("CXXFLAGS+=-O3 -mtune=native -march=native")),
      quiet = drake::ignore(FALSE)) %>%
      list()),
  d = drake::target(
    list(
      maked(n=n, n_amplitude=n_amplitude) %>%
        dplyr::mutate(n=max(trial),j=j)),
    dynamic = cross(n,j,n_amplitude)),
  full = drake::target(c(TRUE,FALSE)),
  fit0 = drake::target(
    d[[1]] %>%
      dplyr::group_nest(n, j) %>%
      dplyr::mutate(
        full = full,
        post = purrr::map2(
          data, full,
          ~do_stan(d=.x, full=.y, stanmodel[[1]]))),
    dynamic = cross(d, full, stanmodel),
    hpc = FALSE),
  ic = drake::target(
    fit0 %>%
      dplyr::mutate(
        psis = purrr::map2(
          post, data,
          loo_power,
          cores = drake::ignore(5)),
        n_amp = purrr::map_dbl(data, ~length(.x$amplitudes[[1]])) ) %>%
      dplyr::select(-post, -data),
    dynamic = map(fit0),
    hpc = FALSE),
  icall = drake::target(
    ic %>%
      tidyr::pivot_wider(names_from=full, values_from = psis)),
  comps = drake::target(
    icall %>%
      dplyr::mutate(
        comp = purrr::map2(
          `TRUE`, `FALSE`,
          loo::loo_compare),
        bf = purrr::map2(
          `TRUE`, `FALSE`,
          ~loo::loo_model_weights(
            list(.x,.y),
            method = "pseudobma"))),
    dynamic = map(icall)),
  out = drake::target(
    comps %>%
      dplyr::select(n,j,n_amp,comp,bf) %>%
      dplyr::mutate(
        elpddiff_full = purrr::map_dbl(comp, ~.x['model1','elpd_diff']),
        elpddiff_reduced = purrr::map_dbl(comp, ~.x['model2','elpd_diff']),
        sediff_full = purrr::map_dbl(comp, ~.x['model1','se_diff']),
        sediff_reduced = purrr::map_dbl(comp, ~.x['model2','se_diff']),
        winner = dplyr::case_when(
          (elpddiff_full == 0) & (abs(elpddiff_reduced) > 3*sediff_reduced) ~ "full",
          (elpddiff_reduced == 0) & (abs(elpddiff_full) > 3*sediff_full) ~ "reduced",
          TRUE ~ "tie"),
        success = stringr::str_detect(winner,"full")) %>%
      dplyr::select(n, j, n_amp, winner,success),
    format = "fst",
    dynamic = map(comps)),
  diagnostics = drake::target(
    fit0 %>%
      dplyr::mutate(
        divergences = purrr::map_dbl(
          post,
          ~.x$sampler_diagnostics()[,,"divergent__"] %>%
            sum()),
        n_amp = purrr::map_dbl(data, ~length(.x$amplitudes[[1]]))) %>%
      dplyr::select(n, n_amp, j, full, divergences),
    dynamic = map(fit0),
    format = "fst"))

cache_path <- here::here("data-raw","caches","power")
if(!fs::dir_exists(cache_path)) drake::new_cache(path=cache_path)
cache <- drake::drake_cache(cache_path)

con <- drake::drake_config(
  plan,
  cache = cache,
  lock_envir = FALSE,
  jobs = 7,
  parallelism = "clustermq",
  caching = "worker",
  memory_strategy = "autoclean",
  verbose = 2,
  garbage_collection = TRUE,
  format = "qs",
  jobs_preprocess = 7,
  recover = TRUE)

