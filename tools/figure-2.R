library(tidyverse)

dvm <- function(x,
                a = 1,
                w = 1/20){
  tmp <- sqrt(1 + 4 * w^2)
  speak <- (2 * sqrt(-2*w + tmp)) / (1 - 2 * w + tmp)
  cpeak <- (-1 + tmp) / (2 * w)
  return(a * sin(x) * exp(w * (cos(x) - cpeak)) / speak)
}

f <- crossing(
  x = seq(-pi, pi, length.out = 1000),
  r = 2,
  a = 1,
  w = c(0.1, 1, 1.001),
  w0 = 5) %>%
  mutate(
    neg = -dvm(x, a, w0 * w),
    pos = dvm(x, a*r, w0),
    neg = if_else(w == 1.001, neg*2, neg),
    y = pos + neg,
    a = factor(a),
    r = factor(r),
    w = factor(w, levels = c("1","0.1","5")),
    x = CircStats::deg(x)/2) %>%
  pivot_longer(cols=c(neg,pos,y), names_to="type") %>%
  mutate(
    type = factor(
      type,
      levels=c("pos", "neg", "y"),
      labels = c("Attractive (Latent)", "Repulsive (Latent)","Sum (Observed)"))) %>%
  rename(
    relative_amplitude = r,
    relative_width = w) %>%
  ggplot(aes(x=x, y=value)) +
  geom_line() +
  facet_grid(
    relative_width~type,
    scales = "free_y") +
  scale_x_continuous(
    name = "Relative Response/Orientation\n of Previous Trial",
    breaks = seq(-90, 90, length.out = 3),
    labels = c("CCW", 0, "CW")) +
  scale_y_continuous(
    name = "Error on Current Trial",
    breaks = seq(-2, 2, length.out = 3),
    labels = c("CCW", 0, "CW")) +
  theme_gray(
    base_size = 8,
    base_family = "ArialMT") +
  theme(
    strip.background.y = element_blank(),
    strip.text.y = element_blank())

ragg::agg_png(
  filename = here::here("figures", "dvm-sums.png"),
  res = 600,
  height = 3,
  width = 5,
  units = "in")
f
dev.off()

