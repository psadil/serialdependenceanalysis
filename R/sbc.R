
sim_participant <- function(d){
  a <- unique(d$amplitude)[[1]]
  w <- unique(d$width)[[1]]
  N <- nrow(d)

  mu <- d %>%
    dplyr::mutate(
      mu = .data$orientation +
        .data$bias +
        sin(2*.data$orientation) * .data$oblique +
        dvm3(.data$orientation_diff, .env$a[1], .env$w[1])) %>%
    magrittr::use_series(mu)

  for(i in 2:N){
    response_diff = atan2(sin(d$orientation[i] - mu[i-1]), cos(d$orientation[i] - mu[i-1]))
    mu[i] <- mu[i] + dvm3(response_diff, a[2], w[2]);
  }
  d$response <- Map(CircStats::rvm, rep.int(1, N), mu - pi, d$kappa) %>%
    simplify2array() %>%
    magrittr::subtract(pi)
  return(d)
}

sim_d <- function(n_per_sub, n_sub){

  d <- tidyr::crossing(sub = factor(1:n_sub), trial = 1:n_per_sub, block = factor(1)) %>%
    dplyr::mutate(
      orientation = runif(dplyr::n(), -pi, pi),
      orientation_last = dplyr::lag(.data$orientation, default = 0),
      orientation_diff = atan2(sin(.data$orientation - .data$orientation_last), cos(.data$orientation - .data$orientation_last)),
      amplitude = list(stats::rnorm(2, 0, 0.25)),
      width = list(stats::rgamma(2, 2, 0.5)),
      oblique = stats::rnorm(1, 0, 0.25),
      bias = stats::rnorm(1, 0, 0.25),
      kappa = extraDistr::rtnorm(1, 10, 10, a=0)) %>%
    dplyr::group_nest(.data$sub) %>%
    dplyr::mutate(data = purrr::map(.data$data, sim_participant)) %>%
    tidyr::unnest(cols = .data$data)

  return(d)
}

compose_d <- function(d){
  n_per_block <- d %>%
    dplyr::group_nest(.data$sub, .data$block) %>%
    dplyr::mutate(n = purrr::map_dbl(.data$data, nrow)) %>%
    dplyr::select(-.data$data) %>%
    tidyr::pivot_wider(names_from = .data$sub, values_from = .data$n, values_fill = list(n=0)) %>%
    dplyr::select(-.data$block) %>%
    as.matrix() %>%
    t()

  n_block_per_sub <- d %>%
    dplyr::group_nest(.data$sub) %>%
    dplyr::mutate(n = purrr::map_dbl(.data$data, ~dplyr::n_distinct(.x$block))) %>%
    magrittr::use_series(n) %>%
    as.array()

  stand <- d %>%
    tidybayes::compose_data() %>%
    c(
      n_per_block = list(n_per_block),
      n_block_per_sub = list(n_block_per_sub))

  return(stand)
}


sbc_i <- function(d, m,
                  seed = NULL,
                  num_samples = 512 - 1,
                  init_thin = 2,
                  max_thin = 64,
                  n_eff_reltol = 0.2,
                  output_dir = NULL,
                  run_ids = 1){

  thin <- init_thin
  while (TRUE) {
    stand <- compose_d(d)
    post <- m$sample(
      data = stand,
      seed = seed,
      num_chains = 1,
      num_cores = 1,
      refresh = 0,
      num_samples = num_samples * thin,
      # num_warmup = 1000 * thin,
      thin = thin,
      output_dir = output_dir,
      run_ids = run_ids,
      init = 1)
    fit_summary <- post$summary()
    n_eff <- fit_summary %>%
      dplyr::filter(stringr::str_detect(.data$variable, "lp__")) %>%
      magrittr::use_series("ess_bulk")
    if (is.na(n_eff)) n_eff <- 0
    if (((2*thin) > max_thin) || dplyr::between(n_eff/num_samples, 1-n_eff_reltol, 1+n_eff_reltol)) break
    thin <- 2 * thin
  }

  prior <- d %>%
    dplyr::mutate(
      `amplitude[1]` = purrr::map_dbl(.data$amplitude, 1),
      `amplitude[2]` = purrr::map_dbl(.data$amplitude, 2),
      `width[1]` = purrr::map_dbl(.data$width, 1),
      `width[2]` = purrr::map_dbl(.data$width, 2)) %>%
    dplyr::distinct(`amplitude[1]`, `amplitude[2]`, `width[1]`, `width[2]`, oblique, bias, kappa) %>%
    tidyr::pivot_longer(
      tidyselect::everything(),
      names_to = "variable",
      values_to = "prior")

  lt_sim <- post$draws() %>%
    posterior::as_draws_df() %>%
    dplyr::select(-lp__, -tidyselect::matches("kappa\\[")) %>%
    tidyr::pivot_longer(
      cols = -c(.chain, .iteration, .draw),
      names_to = "variable",
      values_to = "estimate") %>%
    dplyr::left_join(prior, by = "variable") %>%
    dplyr::mutate(lt = .data$estimate < .data$prior) %>%
    dplyr::mutate(thin = thin)
  return(lt_sim)
}


#' @param y sequence of ranks in 1:max_rank
#' @param max_rank maximum rank of data in y
#' @param bins (default 20): bins to use for chi-square test
#' @error return NA if max rank not divisible by number of bins
#'
#' @return p-value for chi-square test that data is evenly
#' distributed among the bins
test_uniform_ranks <- function(y, max_rank, bins = 20) {
  if (max_rank / bins != floor(max_rank / bins)) {
    # printf("ERROR in test_uniform_ranks")
    # printf(" max rank must be divisible by bins.")
    # printf(" found max rank = %d; bins = %d", max_rank, bins)
    return(NA)
  }
  bin_size <- max_rank / bins
  bin_count <- rep(0, bins)
  N <- length(y)
  for (n in 1:N) {
    bin <- ceiling(y[n] / bin_size)
    bin_count[bin] <- bin_count[bin] + 1
  }
  chisq.test(bin_count)$p.value
}
