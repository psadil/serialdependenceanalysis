
residualize <- function(d, .f = error~1){
  .f <- enquo(.f)

  stopifnot(exprs = {
    all(c("sub", "error") %in% names(d))
  })

  out <- d %>%
    dplyr::group_nest(.data$sub) %>%
    dplyr::mutate(
      fit = purrr::map(
        data,
        ~lm(!!.f, data=.x)),
      data = purrr::map2(
        data, fit,
        ~.x %>% dplyr::mutate(error0 = residuals(.y)))) %>%
    # dplyr::select(-fit) %>%
    tidyr::unnest(cols = data) %>%
    dplyr::mutate(
      error = dplyr::case_when(
        error0 < -pi ~ 2*pi + error0,
        error0 > pi ~ 2*pi - error0,
        TRUE ~ error0)) %>%
    dplyr::ungroup() %>%
    dplyr::select(-error0)

  return(out)
}


