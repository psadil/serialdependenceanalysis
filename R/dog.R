
dog <- function(x,
                a = 1,
                b = 1/20,
                c = sqrt(2*exp(1))){
  return(a * b * c * x * exp(-(b*x)^2))
}

dvm <- function(x,
                a = 1,
                w = 1/20){
  return(a * w * sin(x) * exp(w * cos(x)) / (2*pi*besselI(w, 0)))
}

dvm2 <- function(x,
                 a = 1,
                 w = 1/20){
  normalizer <- dvm(2*atan(sqrt(sqrt(4*w^2+1) - 2*w)),1,w)
  return(a * w * sin(x) * exp(w * cos(x)) / (besselI(w, 0)*normalizer))
}

dvm3 <- function(x,
                 a = 1,
                 w = 1/20){
  tmp <- sqrt(1 + 4 * w^2)
  speak <- (1 - 2 * w + tmp) / (2 * sqrt(-2*w + tmp))
  cpeak <- (-1 + tmp) / (2 * w)
  return(a * sin(x) * exp(w * (cos(x) - cpeak)) * speak)
}


initdog <- function(mCall, data, LHS){
  xy <- data.frame(sortedXyData(mCall[["input"]], LHS, data))
  z <- xy[["y"]]
  pars <- c("alpha" = sign(mean(z)), sigma = log(1/20))
  return(pars)
}

SSdog <- selfStart(
  ~alpha * sigma * input * sqrt(2*exp(1))*exp(-(sigma * input)^2),
  initial = initdog,
  parameters = c("alpha", "sigma"))

fit_nls_on_bootstrap <- function(d, start) {
  fit <- nls(
    formula = error ~ SSdog(orientation_diff, alpha, sigma),
    data = d,
    start = c(alpha = sign(mean(d$error)), sigma = 1/20),
    lower = c(alpha=-Inf, sigma=0),
    upper = c(alpha=Inf, sigma=1),
    algorithm = "port",
    control = list(maxiter = 500, warnOnly = TRUE, minFactor = 1/2048))
  # fit <- nls(
  #   formula = error ~ SSdog(orientation_diff, alpha, sigma),
  #   data = d,
  #   start = list(alpha = sign(mean(d$error)), sigma = log(1/20)),
  #   control = list(maxiter = 500, warnOnly = TRUE, minFactor = 1/2048))
  return(fit)
}
