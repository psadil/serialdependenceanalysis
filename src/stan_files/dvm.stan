functions {
  #include functions/functions.stan
}
data {
  #include data/dvm.stan
}
transformed data{
  real y[n] = to_array_1d(error);
  vector[n] sine2o = sin(orientation*2);
  matrix[n, 2] z[n_diffs];
  for (i in 1:n_diffs) z[i] = append_col(cos(diffs[,i]), sin(diffs[,i]));
}
parameters {
  #include parameters/dvm_parameters.stan
  #include parameters/sigma.stan
}
model {
  matrix[n, n_diffs] angles;
  vector[n] mu = bias + (oblique * sine2o);
  for(i in 1:n_diffs) angles[,i] = dvmhier(z[i], [amplitude[i]]', [width[i]]', {1});
  for(i in 1:n) {
    if (skip[i] > 0){
      angles[i,] = rep_row_vector(0, n_diffs);
    }
  }
  for(i in 1:n_diffs) mu += angles[,i];

  amplitude ~ normal(0, 0.25);
  width ~ gamma(2, 0.5);
  bias ~ normal(0, 0.25);
  oblique ~ normal(0, 0.25);
  sigma_loc ~ std_normal();
  sigma_scale ~ gamma(2, 1);
  sigma ~ normal(sigma_loc, sigma_scale);
  target += -normal_lccdf(0 | sigma_loc, sigma_scale) * n_sub;


  error ~ normal(mu, sigma[sub]);
}
