functions {
  #include functions/functions.stan
  // real partial_sum(
  //   real[] slice_y,
  //   int start, int end,
  //   int[] sub,
  //   vector mu, vector sigma){
  //     return normal_lpdf(slice_y | mu[start:end], sigma[sub[start:end]]);
  //   }
}
data {
  int<lower=0> n;
  vector<lower=-pi(), upper=pi()>[n] error;
  vector<lower=0, upper=2*pi()>[n] orientation;
  int<lower=1, upper=2> n_diffs;
  matrix<lower=-pi(), upper=pi()>[n, n_diffs] diffs;
  int n_sub;
  int<lower=0, upper=n_sub> sub[n];
  int<lower=0, upper=1> skip[n];
}
transformed data{
  real y[n] = to_array_1d(error);
  vector[n] sine2o = sin(orientation*2);
  matrix[n, 2] z[n_diffs];
  for (i in 1:n_diffs) z[i] = append_col(cos(diffs[,i]), sin(diffs[,i]));
}
parameters {
  #include parameters/dvm-hier_parameters.stan
  #include parameters/sigma.stan
}
transformed parameters{
  vector<lower=0>[n_sub] sigma = sigma_loc + sigma_raw * sigma_scale;
  matrix[n_sub, n_diffs] amplitude;
  for(i in 1:n_diffs) amplitude[,i] = amplitude_loc[i] + amplitude_raw[,i]*amplitude_scale[i];
}
model {
  matrix[n, n_diffs] angles;
  vector[n] mu = bias[sub] + (oblique[sub] .* sine2o);
  for(i in 1:n_diffs) angles[,i] = dvmhier(z[i], amplitude[,i], width[,i], sub);
  for(i in 1:n) {
    if (skip[i] > 0){
      angles[i,] = rep_row_vector(0, n_diffs);
    }
  }
  for(i in 1:n_diffs) mu += angles[,i];

  #include model/dvm-hier_priors.stan
  #include model/sigma.stan

  // target += reduce_sum(partial_sum, y, 1, sub, mu, sigma);
  error ~ normal(mu, sigma[sub]);
}
