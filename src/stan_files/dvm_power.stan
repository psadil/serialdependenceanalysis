functions {
vector dvm(matrix x, real width){
  real peak = 2*atan(sqrt(fma(-2, width, sqrt(fma(square(width), 4, 1)))));
  real speak = sin(peak);
  real cpeak = cos(peak);
  return x[,2] .* exp(width * (x[,1] - cpeak)) / speak;
}
}
data {
  int<lower=0> n;
  vector[n] error;
  vector[n] orientation_diff;
  vector[n] response_diff;
  int n_condition;
}
transformed data{
  matrix[n,2] zo = append_col(cos(orientation_diff), sin(orientation_diff));
  matrix[n,2] zr = append_col(cos(response_diff), sin(response_diff));
}
parameters {
  vector[n_condition] amplitude;
  vector[n_condition] width;
  real<lower=0> sigma;
}
model {
  matrix[n, n_condition] x;
  x[,1] = dvm(zo, width[1]);
  if(n_condition == 2) x[,2] = dvm(zr, width[2]);

  amplitude ~ normal(0, 0.5);
  width ~ gamma(2, 0.5);
  sigma ~ gamma(2, 5);
  error ~ normal(x * amplitude, sigma);
}
