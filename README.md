
# serialdependenceanalysis

This repository holds analysis scripts. The raw data are stored as a submodule, at `data-raw/serialdependence`.

## Dependencies

This repository relies on the R package [renv](https://github.com/rstudio/renv). Upon opening the project file (`serialdependenceanalysis.Rproj`) in Rstudio, you will be prompted to install the required `R` dependencies.

Note that these analyses rely on the `R` interface to `CmdStan`, called [CmdStanR](https://github.com/stan-dev/cmdstanr). At the time of writing, `renv` cannot handle the dependency on `CmdStan`. If you intend to rerun the analyses, you will need to separately install `CmdStan`. Instructions for doing this can be found on the [`CmdStanR` website](https://mc-stan.org/cmdstanr/articles/cmdstanr.html).

## Installation

There is no need to install this repo as an R package. Instead, it should be cloned. Scripts to make each of the figures are in the `tools/` folder. Those scripts rely on [drake](https://docs.ropensci.org/drake/) caches. Workflows to generate the caches are in `tools/wf_model-dvm-hier.R` and `tools/wf_power.R`.

```r
drake::r_make('tools/wf_model-dvm-hier.R')
drake::r_make('tools/wf_power.R')
```

Note that these workflows will take a few hours, and if you want to run them you may need to tweak the parallelization. 

The output of the workflows are stored as gzipped archives, connected to the OSF repository. If you would like to generate the figures without re-running the analyses, the archives should be unpacked in the appropriate folder inside `data-raw/caches`.

## Data

A copy of the raw data files for each of the analyzed experiments are stored in `data-raw`. 

- `data-raw/fw2.csv`: Fischer & Whitney (2014)
- `data-raw/Experiment*_rawdata.csv` Pascucci et al. (2019)
- `data-raw/lit.csv`: Samaha et al. (2019)

They are complied into a binary, `data-raw/experiments.fst` with the script `data-raw/all.R`. This output, `experiments.fst`, is used in the workflow for running the Bayesian analysis (`data-raw/wf_model-dvm-hier.R`).
