library(tidyverse)
devtools::load_all()

files_maskedclick <- fs::dir_ls(
  path = here::here("data-raw","serialdependence","events", "maskedclick"),
  regexp = "sub.*task-serialdependence.*data\\.csv$",
  recurse = FALSE)

maskedclick <- vroom::vroom(
  files_maskedclick,
  col_types = list(
    subject = "n",
    session = "n",
    block = "n",
    section = "n",
    contrast_db = "f")) %>%
  select(subject:rt, -contrast_db, -contrast, -isi_raw_sec, -session) %>%
  mutate(experiment = "maskedclick") %>%
  rename(sub = subject)

samaha <- vroom::vroom(
  "data-raw/lit.csv",
  col_types = list(
    sub = "n",
    block = "n",
    trialnum = "n",
    delay = "f",
    posev = "f",
    conf = "f")) %>%
  rename(
    orientation = ori,
    response = resp,
    error = dist,
    trial = trialnum) %>%
  mutate(
    section = 1,
    experiment = "samaha",
    response = response - 180)

files_pas <- fs::dir_ls(
  path = here::here("data-raw"),
  regexp = "Experiment(1|2).*rawdata\\.csv$",
  recurse = FALSE)

pas <- vroom::vroom(
  files_pas,
  col_types = list(
    observer = "n"),
  id = "file") %>%
  rename(
    sub = observer,
    response = reported) %>%
  mutate(
    experiment = str_extract(file, "Experiment[[:digit:]]"),
    experiment = str_c("pas", str_extract(experiment, "[[:digit:]]")),
    block = 1,
    section = 1) %>%
  select(-file) %>%
  dplyr::group_by(sub, experiment) %>%
  mutate(trial = 1:n()) %>%
  dplyr::ungroup()

fw <- vroom::vroom(
  here::here("data-raw","fw2.csv")) %>%
  tidyr::extract(
    file,
    into = c("sub", "side", "block"),
    regex = ".*S([[:digit:]])_(left|right)_([[:digit:]]).*",
    convert = TRUE) %>%
  mutate(section = 1) %>%
  # select(-separations) %>%
  mutate(
    experiment = "fw",
    orientation = orientation*-1 + 90,
    response = response*-1 - 90)

main <- bind_rows(fw, maskedclick, pas, samaha) %>%
  dplyr::group_by(sub, side, block, section, experiment) %>%
  mutate(
    last_orientation = dplyr::lag(orientation),
    orientation_diff = great_circ(orientation, last_orientation, range_dist = 180, max_diff = 90, sym = FALSE),
    error = great_circ(orientation, response, range_dist = 180, max_diff = 90, sym = FALSE),
    response_diff = great_circ(orientation, dplyr::lag(response), range_dist = 180, max_diff = 90, sym = FALSE)) %>%
  dplyr::ungroup() %>%
  select(-last_orientation) %>%
  mutate(sub = factor(sub))

fst::write_fst(main, here::here("data-raw", "experiments.fst"))

